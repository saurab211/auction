<?php

namespace App\Http\Controllers;
use App\Supplier;
use App\Bid; 
use App\Auction; 

use Illuminate\Http\Request;

class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $bid = Bid::where('auction_id', $id)->with('auction','supplier')->get();
        return response()->json($bid); 
    }
    

    // get Supplier info
    public function get_supplier_auction($id) {
        $supplier = []; 
        $supplier['supplier'] = Supplier::all();
        $supplier['auction'] = Auction::where('id',$id)->get();
        return $supplier; 

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bid = new Bid();
        $bid->unit_price = $request->unit_price;
        $bid->supplier_id = $request->supplier_id;
        $bid->auction_id = $request->auction_id;

        $bidCheck = Bid::where('supplier_id', $request->supplier_id)->first(); 
        

        if (isset($bidCheck)){
            $bidCheck->unit_price = $request->unit_price;
            if ($bidCheck->save()) {
                

                return response()->json([
                    'status' => true,
                    'message' => 'Bid Item Updated Successfully',
                    ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Bid Item Insert Error, Please try again',
                    ]);
            }
            
        } else {
            if ($bid->save()) {
                return response()->json([
                    'status' => true,
                    'message' => 'Bid Item Added Successfully',
                    ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Bid Item Insert Error, Please try again',
                    ]);
            }

        }



        
    }

}
