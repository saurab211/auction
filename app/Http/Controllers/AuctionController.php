<?php

namespace App\Http\Controllers;

use App\Auction;
use Illuminate\Http\Request;

class AuctionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auctions = Auction::all();

        return $auctions;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $auction = new Auction();

        $auction->description = $request->description;
        $auction->product = $request->product;
        $auction->unit_qty = $request->unit_qty;

        if ($auction->save()) {
            return response()->json([
                'status' => true,
                'message' => 'Auction Item Added Successfully',
                ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Auction Item Insert Error, Please try again',
                ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auction = Auction::find($id);
        return response()->json($auction);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $auction = Auction::where('id', $id)->first();

        $auction->description = $request->description;
        $auction->product = $request->product;
        $auction->unit_qty = $request->unit_qty;

        if ($auction->save()) {
            return response()->json([
                'status' => true,
                'message' => 'Auction Item Updated Successfully',
                ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Auction Item update Error, Please try again',
                ]);
        }




    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $auction = Auction::find($id);
        if($auction->delete()) {
            return response()->json(['status' => true, 'message' => 'Auction Deleted Successfully']);

        } else {
            return response()->json(['status' => false, 'message' => 'Some issue while deleting']);

        }

    }
}
