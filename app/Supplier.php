<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function bids()
    {
        return $this->hasMany('App\Bid');
    }
}
