<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    public function auction()
    {
        return $this->belongsTo('App\Auction');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

}


