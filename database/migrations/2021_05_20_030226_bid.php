<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Bid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->id();
            $table->integer('unit_price');
            $table->unsignedBigInteger('auction_id');
            $table->unsignedBigInteger('supplier_id');

            $table->foreign('auction_id')
            ->references('id')->on('auctions')
            ->onDelete('cascade');

            
            $table->foreign('supplier_id')
            ->references('id')->on('suppliers')
            ->onDelete('cascade');


         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
