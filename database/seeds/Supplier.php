<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class Supplier extends CsvSeeder
{

    public function __construct()

   {
     $this->table = 'suppliers';

     $this->filename = base_path() . '/database/seeded_csv_files/suppliers.csv';

    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
		// DB::disableQueryLog();

		// Uncomment the below to wipe the table clean before populating
		parent::run();
    
    }
}
