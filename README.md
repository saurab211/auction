## **AUCTION SITE README**

## INTRODUCTION

Our client is a PC manufacturer and they have asked us to review their process in spare-parts procurement.

After reviewing their process, we have decided to build a simple auction portal in Laravel + vue.js app for a demonstration.

There are 3 tables here:
_ Auctions (Data provided in auctions.csv)
_ Suppliers (Data provided in suppliers.csv) \* Bids (Transactional table between Supplier and Auction)
Note: We removed the Buyer table to simplify this demo by assuming the portal user is Buyer
version 1.0
CSV file for suuplier & auction is provided in /database/seeded_csv_files

Site major function will be:

-   Sellers bid down price on auction item
-   Buyer compares prices real time

---

## INSTALLATION PROCESS

-   Clone the Repsository
-   go to cloned folder through terminal using cd
-   Create or copy .env.example file to .env file
-   open .env file and give database information (DB_DATABASE, DB_USERNAME, DB_PASSWORD)
-   create a database locally name same as DB_DATABASE
-   Run Command 'composer install'
-   Run php artisian key:generate
-   Run php artisian migrate
-   Run php artisian db:seed
-   Install npm if required
-   Run php artisian serv
-   Run npm run deor npm run watch in another terminal for same directory
-   go to localhost:8000

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages for any updates.

---

## ADMIN

Contact in below email for any issues and I will try to responsd ASAP.

Email: shresthasaurav12@gmail.com
