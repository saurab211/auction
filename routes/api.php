<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


    Route::get('getAuctions', 'AuctionController@index');
    Route::post('SaveAuctionItem', 'AuctionController@create');
    Route::delete('deleteAuction/{id}', 'AuctionController@destroy');
    Route::get('get_auction/{id}','AuctionController@show');
    Route::post('update_auction/{id}', 'AuctionController@edit');

    // get supplier
    Route::get('getAuctionSuppliers/{id}', 'BidController@get_supplier_auction');

    //  bid info
    Route::get('getBid/{id}', 'BidController@index');
    Route::post('SaveBidItem', 'BidController@store');


