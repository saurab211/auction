require('./bootstrap');
window.Vue = require('vue');


import VueRouter from 'vue-router';
Vue.use(VueRouter);


import App from './App.vue';

// import components
import Home from '../js/components/Home';

// auction component
import Auctionlist from '../js/components/auction/Auctionlist';
import AddAuction from '../js/components/auction/AddAuction';
import EditAuction from '../js/components/auction/EditAuction';

// bid info compononent
import BidInfo from '../js/components/bid/BidInfo';



// sweet alert
import VueSweetalert2 from 'vue-sweetalert2';
window.Swal = require('sweetalert2');
Vue.use(VueSweetalert2);

import utils from './helpers/utilities';
Vue.prototype.$utils = utils


import VueAxios from 'vue-axios';
import axios from 'axios';
import Vue from 'vue';
Vue.use(VueAxios, axios);

const routes = [
    {
        name: '/',
        path: '/',
        component: Home
    },

    {
        name: '/auction',
        path: '/auction',
        component: Auctionlist
    },

    {
        name: '/add_auction',
        path: '/add_auction',
        component: AddAuction
    },
    {
        name: '/get_auction',
        path: '/get_auction/edit/:id?',
        component: EditAuction
    },
    
    {
        name: '/show_bid',
        path: '/show_bid/detail/:id?',
        component: BidInfo
    },
    {
        name: '/add_bid',
        path: '/add_bid',
        component: BidInfo
    },


];



const router = new VueRouter({mode: 'history',routes: routes});
const app = new Vue(Vue.util.extend({router }, App)).$mount('#app');

